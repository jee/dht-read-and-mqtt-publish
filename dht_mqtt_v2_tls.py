#!/usr/bin/python3

import time
import board
import adafruit_dht
import paho.mqtt.publish as publish

 
# INIT DHT DEVICE, DATA WIRE CONNECTED TO PIN 4 (D4):
dhtDevice = adafruit_dht.DHT22(board.D4)

# POLLRATE FOR THE SENSOR (BELOW 2 SEC IS NOT RECOMMENDED)
pollRate = 30.0

# The maximum difference between two measurements
maxDif = 1 

Broker = '$server-address.com'
BrokerPort = $Port #8883 for TLS, 1883 for unsecured)
pubTopicTemp = 'room_1/temperature'
pubTopicHum = 'room_1/humidity'
auth = {'username': '$username',
        'password': '$password'
}

while True:
        try:
                # COLLECT SENSOR DATA
                ## Make first measurement
                temperature = dhtDevice.temperature
                humidity = dhtDevice.humidity

                # WAIT
                time.sleep(4.0)

                ## Make second measurement
                temperature2 = dhtDevice.temperature
                humidity2 = dhtDevice.humidity

                # Compare if the two measurements do not differ too much 
                if (abs(temperature - temperature2) < maxDif) and (abs(humidity - humidity2) < maxDif):
                        publish.single(pubTopicTemp, str(temperature),
                                hostname=Broker, port=BrokerPort,
                                auth=auth, tls={})

                        publish.single(pubTopicHum, str(humidity),
                                hostname=Broker, port=BrokerPort,
                                auth=auth, tls={})
                        time.sleep(pollRate)
                else:
                    time.sleep(4.0)

        except RuntimeError as error:
        # Errors happen fairly often, DHT's are hard to read, just keep going
                print(error.args[0])




