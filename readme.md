# DHT MQTT

Python scripts to read data from a DHT sensor (on a raspberry pi for example) and publish it to a MQTT broker over TLS or unsecured.
To prevent spikes in graphs it checks if the collected data is within reasonable range.

## Version 2
Version 2 of the script checks the measurements against a previous measurement to ensure somewhat reliable data.

If the first measurement and the second measurement are within the margin of error (default=1), the data gets send. If the difference is greater, the measurement is repeated. 
This avoids temperature jumps through errornous reading like 23° -> 84° -> 23.1°. 

### 2 Variants
* dht_mqtt_v2_tls.py - The updated file

* dht_mqtt_v2_no_TLS.py - Same as above but with debug lines and without TLS.
---

## Dependencies

The script depends on:

- [paho-mqtt](https://pypi.org/project/paho-mqtt/)
- [Adafruit CircuitPython](https://github.com/adafruit/circuitpython)

There is a nice tutorial for how to set up the data reading part of the script here:

- https://learn.adafruit.com/circuitpython-on-raspberrypi-linux/installing-circuitpython-on-raspberry-pi

---

## Acknowledgement and License

This code is inspired by:

- https://www.earth.li/~noodles/blog/2018/05/rpi-mqtt-temp.html
- https://thingsboard.io/docs/samples/raspberry/temperature/
- https://github.com/adafruit/Adafruit_CircuitPython_DHT/blob/master/examples/dht_simpletest.py

Thus I assume the Apache 2.0 License applies, which is [available here](https://apache.org/licenses/LICENSE-2.0)

---

## Feedback

Is welcome.

---

## Running the Script as Systemd-Unit at Startup
Adjust the settings in dht-mqtt.service. Then copy the file to /etc/systemd/service

then

* sudo systemctl deamon-reload
* sudo systemctl start dht-mqtt

check if it's running without problems:

* sudo systemctl status dht-mqtt

if so, enable it to run at boot:

* sudo systemctl enable dht-mqtt
