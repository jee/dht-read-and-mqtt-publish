#!/usr/bin/python3

import time
import board
import adafruit_dht
import paho.mqtt.publish as publish

# INIT DHT DEVICE, DATA WIRE CONNECTED TO PIN 4 (D4):

dhtDevice = adafruit_dht.DHT22(board.D4)

# POLLRATE FOR THE SENSOR (BELOW 2 SEC IS NOT RECOMMENDED)
pollRate = 30.0

maxDif = 1 # The maximum difference between two measurements

# Set Address and Port of MQTT Server
Broker = '$server-address.com'
BrokerPort = $Portnumber # 1883 for unsecured

# Set Topics to be published
pubTopicTemp = 'room_1/temperature'
pubTopicHum = 'room_1/humidity'

# Authentification
auth = {'username': 'username',
        'password': 'password'
}

while True:
        try:
                # COLLECT SENSOR DATA
                ## Make first measurement
                temperature = dhtDevice.temperature
                humidity = dhtDevice.humidity
 #               print('first sample collected')
                # WAIT
                time.sleep(4.0)

                ## Make second measurement
                temperature2 = dhtDevice.temperature
                humidity2 = dhtDevice.humidity
#                print('second sample collected')

                # Compare if the two measurements do not differ too much 
                if (abs(temperature - temperature2) < maxDif) and (abs(humidity - humidity2) < maxDif):
#                        print('about to send payloads')
                        publish.single(pubTopicTemp, str(temperature),
                                hostname=Broker, port=BrokerPort,
                                auth=auth)
#                        print('first payload temperature sent')
                        publish.single(pubTopicHum, str(humidity),
                                hostname=Broker, port=BrokerPort,
                                auth=auth)

#                        print('second payload humidity sent')
                        time.sleep(pollRate)

                else:
                        time.sleep(4.0)

        except RuntimeError as error:
        # Errors happen fairly often, DHT's are hard to read, just keep going
                print(error.args[0])

