#!/usr/bin/python3

import time
import board
import adafruit_dht
import paho.mqtt.publish as publish

 
# INIT DHT DEVICE, DATA WIRE CONNECTED TO PIN 4 (D4):

dhtDevice = adafruit_dht.DHT22(board.D4)

# POLLRATE FOR THE SENSOER (BELOW 2 SEC IS NOT RECOMMENDED)
pollRate = 30.0

# SETTING RANGE OF EXPECTED DATA VALUES

tempLow = 5 # Temperature is unlikely to fall below 
tempHi = 60 # Temperature limit is unlikely to rise above 

humLow = 0  # Humidity is unlikely to fall below 
humHi = 100  # Humidity is unlikely to rise above 
 
Broker = 'yourserver.com'
pubTopicTemp = 'room_1/temperature'
pubTopicHum = 'room_1/humidity'
auth = {'username': 'yourUsername',
        'password': 'yourPassword'
}

while True:
    try:
        # COLLECT SENSOR DATA

        temperature = dhtDevice.temperature
        humidity = dhtDevice.humidity

# REMOVE HASH TO PRINT VALUES TO CONSOLE / LOG 
#        print("Temp: {:.1f} C    Humidity: {}% "
#              .format(temperature, humidity))
        
# SEND SENSOR DATA TO MQTT IF DATA IS REASONABLE

        if (tempLow <= temperature <= tempHi) and (humLow <= humidity <= humHi):
# REMOVE HASH FOR DEUG
#          print("sending data")
          publish.single(pubTopicTemp, str(temperature),
                  hostname=Broker, port=8883,
                  auth=auth, tls={})

          publish.single(pubTopicHum, str(humidity),
                  hostname=Broker, port=8883,
                  auth=auth, tls={})

# WAIT TO LIMIT COLLECTED DATA / NOT OVERLOAD THE SENSOR

          time.sleep(pollRate)

# IF DATA IS NOT WITHIN EXPECTED VALUES, JUST WAIT AND RETRY
        else:           

# REMOVE HASH FOR DEBUG 
#          print("Data values are out of limits")
#          print("Temp: {:.1f} C    Humidity: {}% "
#                .format(temperature, humidity))
          time.sleep(2.0)


    except RuntimeError as error:
        # Errors happen fairly often, DHT's are hard to read, just keep going
        print(error.args[0])
 
time.sleep(2.0)  
